package com.swesource.arconsole;

/**
 * Workaround for getting JavaFX-application execute as fat-jar.
 * See: https://stackoverflow.com/questions/56894627/how-to-fix-error-javafx-runtime-components-are-missing-and-are-required-to-ru?rq=1
 */
public class Application {

    public static void main(final String[] argv) {
        MainApp.main(argv);
    }
}
