package com.swesource.arconsole.controller;

import com.swesource.arconsole.model.Node;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Logger;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class FXMLController implements Initializable {

    private static final Logger LOGGER = Logger.getLogger("com.swesource.arconsole.controller.FXMLController");
    
    private Stage stage;
    private ArrayList<String> binaryExt;
    private ArrayList<String> archiveExt;

    @FXML
    private TabPane tabPane;
    
    @FXML
    private Tab infoTab;

    @FXML
    private Tab treeTab;

    @FXML
    private Tab fileTab;
    
    @FXML
    private TextArea fileText;
    
    @FXML
    private TextField statusTextField;

    @FXML
    private TreeTableView<Node> treeTable;

    @FXML
    private TreeTableColumn<Node, String> nameColumn;

    @FXML
    private TreeTableColumn<Node, String> typeColumn;

    @FXML
    private TreeTableColumn<Node, String> packageColumn;

    @FXML
    private TreeTableColumn<Node, String> sizeColumn;

    @FXML
    private TextArea leftInfoText;

    @FXML
    private TextField leftInfoTextFieldFilename;

    @FXML
    private TextField leftInfoTextFieldPath;

    @FXML
    private TextField leftInfoTextFieldSize;

    @FXML
    private TextField leftInfoTextFieldEntries;

    @FXML
    private TextField leftInfoTextFieldComment;

    @FXML
    private TextField rightInfoTextFieldFilename;

    @FXML
    private TextField rightInfoTextFieldPath;

    @FXML
    private TextField rightInfoTextFieldSize;

    @FXML
    private TextField rightInfoTextFieldEntries;

    @FXML
    private TextField rightInfoTextFieldComment;

    /*
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        archiveExt= new ArrayList<>();
        archiveExt.add(".ear");
        archiveExt.add(".jar");
        archiveExt.add(".war");
        binaryExt = new ArrayList<>();
        binaryExt.add(".class");
        binaryExt.add(".ear");
        binaryExt.add(".jar");
        binaryExt.add(".war");
        addKeysToLeftInfoPane();
    }

    @FXML
    public void quit(ActionEvent event) {
        System.exit(0);
    }
    
    @FXML
    public void menuAbout(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/infoPane.fxml"));
        Stage aboutStage = new Stage();
        DialogPane pane = (DialogPane) loader.load();
        aboutStage.setScene(new Scene(pane));
        aboutStage.setTitle("About");
        aboutStage.initModality(Modality.APPLICATION_MODAL);
        aboutStage.setResizable(false);
        InfoPaneController controller = (InfoPaneController) loader.getController();
        controller.setStage(aboutStage);
        controller.setInfoType("about");
        aboutStage.show();
    }

    @FXML
    private void onDragDropped(DragEvent event) {
        System.out.println("here");
    }
    
    @FXML
    private void menuOpenArchive(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        // TODO: The extension filter doesn't seems to work!
        List extensions = new ArrayList<>();
        extensions.add("*.ear");
        extensions.add("*.war");
        extensions.add("*.jar");
        FileChooser.ExtensionFilter filter
                = new FileChooser.ExtensionFilter("archives", extensions);
        fileChooser.setSelectedExtensionFilter(filter);
        File file = fileChooser.showOpenDialog(stage);
        openArchive(file);
    }
    
    public void openArchive(File file) {
        if (file != null) {
            try {
                JarFile jar = new JarFile(file);
                TreeItem treeRoot = addRootNode(file, jar);
                addDataToRightInfoPane(treeRoot);
            } catch (IOException e) {
                System.out.println("JarFile not working.");
                //Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //tabPane.getSelectionModel().select(treeTab);
    }

    private TreeItem addRootNode(File file, JarFile jarFile) {
        String jarFullFileName = jarFile.getName();
        JarEntry jarEntry = new JarEntry(jarFullFileName);
        jarEntry.setSize(file.length());
        Node<JarEntry, JarFile> node = new Node(jarEntry, jarFile);
        int index = jarFullFileName.lastIndexOf("/");
        String jarFileName = jarFullFileName.substring(index + 1);
        String jarFilePath = jarFullFileName.substring(0, index);
        node.setName(jarFileName);
        node.setPkg(jarFilePath);

        TreeItem<Node> rootTreeItem = new TreeItem(node);
        treeTable.setRoot(rootTreeItem);
        treeTable.setOnMouseClicked((MouseEvent event) -> {
            //public void handle(MouseEvent mouseEvent) {
            //if(event.getButton().equals(MouseButton.PRIMARY)){
            if (event.getClickCount()>1) { // ==2
                Object o = event.getSource();
                TreeItem ti = treeTable.getSelectionModel().selectedItemProperty().getValue();
                Node n = (Node)ti.getValue();
                showFileContent(n);
                // Open the file tab
                tabPane.getSelectionModel().select(fileTab);
            }
        });

        nameColumn.setCellValueFactory(
                (TreeTableColumn.CellDataFeatures<Node, String> param)
                -> new ReadOnlyStringWrapper(param.getValue().getValue().getName())
        );

        typeColumn.setCellValueFactory(
                (TreeTableColumn.CellDataFeatures<Node, String> param)
                -> new ReadOnlyStringWrapper(param.getValue().getValue().getExt())
        );

        packageColumn.setCellValueFactory(
                (TreeTableColumn.CellDataFeatures<Node, String> param)
                -> new ReadOnlyStringWrapper(param.getValue().getValue().getPkg())
        );

        sizeColumn.setCellValueFactory(
                (TreeTableColumn.CellDataFeatures<Node, String> param)
                -> new ReadOnlyStringWrapper(Long.toString(((JarEntry) (param.getValue().getValue().getEntry())).getSize()))
        );

        try {
            addNodesRecursively(jarFile, rootTreeItem);
        } catch (IOException e) {
            System.out.println("FEL adding nodes!");
            //Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        statusTextField.setText(jarFullFileName);
        return rootTreeItem;
    }

    private void addNodesRecursively(JarFile jarFile, TreeItem<Node> rootTreeItem) throws IOException {
        Enumeration entries = jarFile.entries();
        ArrayList entriesList = Collections.list(entries);
        for (Object entry : entriesList.toArray()) {
            JarEntry jarEntry = (JarEntry) entry;
            if (jarEntry.isDirectory()) {
                //System.out.println(je.getName());
                continue;
            }
            Node<JarEntry, JarFile> node = new Node(jarEntry, jarFile);
            setFileNameDetailsInNode(node);
            TreeItem treeItem = new TreeItem(node);
            rootTreeItem.getChildren().add(treeItem);

            String ext = node.getExt();
            if (ext.matches("(?i:jar)|(?i:war)|(?i:ear)")) {
                JarFile newJarFile = retrieveInternalArchiveFile(jarEntry, jarFile);
                addNodesRecursively(newJarFile, treeItem);
            }
        }
    }

    private void setFileNameDetailsInNode(Node<JarEntry, JarFile> node) {
        JarEntry jarEntry = node.getEntry();
        String fullName = jarEntry.getName();
        // TODO: Make sure delimiter is universal!
        int nameIndex = fullName.lastIndexOf("/");

        String pkg = "";
        if (nameIndex != -1) {
            pkg = fullName.substring(0, nameIndex);
        }

        try {
            pkg = pkg.replaceAll("/", ".");
            node.setPkg(pkg);
        } catch (Exception e) {
            System.out.println("FXMLController.fixName(): " + fullName + ":" + nameIndex);
        }

        int extIndex = fullName.lastIndexOf(".");
        String name = fullName.substring(nameIndex + 1, extIndex);
        node.setName(name);

        String ext = fullName.substring(extIndex + 1);
        node.setExt(ext);
    }

    private JarFile retrieveInternalArchiveFile(JarEntry jarEntry, JarFile jarFile) throws IOException {
        // read jar entry into a byte array
        InputStream in = new BufferedInputStream(jarFile.getInputStream(jarEntry));

        // write these bytes into a file
        File temporary = File.createTempFile("clazzjarfile", ".jar");
        OutputStream out = new BufferedOutputStream(new FileOutputStream(temporary));

        int len;
        byte[] buf = new byte[1024];
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }

        // release resource
        out.flush();
        out.close();
        in.close();

        // analyse the temporary file
        return new JarFile(temporary);
    }
    
    private void showFileContent(Node<JarEntry, JarFile> node) {
        JarEntry jarEntry = node.getEntry();
        if (!jarEntry.getName().endsWith(".class")) { // TODO: list of bin formats
            long size = jarEntry.getSize();
            byte[] buf = new byte[(int) size];
            try {
                JarFile jarFile = node.getContainer();
                //File f = new File(jarFile.getName());
                //JarFile jf = new JarFile(f);
                //InputStream is = jf.getInputStream(jarEntry);
                InputStream is = jarFile.getInputStream(jarEntry);
                BufferedInputStream bis = new BufferedInputStream(is);
                bis.read(buf);
                String s = new String(buf);
                fileText.setText(s);
            }
            catch(IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void addKeysToLeftInfoPane() {
        leftInfoTextFieldFilename.setText("Filename");
        leftInfoTextFieldPath.setText("Path");
        leftInfoTextFieldSize.setText("Size (bytes)");
        leftInfoTextFieldEntries.setText("Entries");
        leftInfoTextFieldComment.setText("Comment");
    }
    
    private void addDataToRightInfoPane(TreeItem<Node<JarEntry, JarFile>> rootTreeItem) {

        Node node = rootTreeItem.getValue();
        JarEntry jarEntry = (JarEntry)node.getEntry();
        JarFile jarFile = (JarFile)node.getContainer();
        rightInfoTextFieldFilename.setText(node.getName());
        rightInfoTextFieldPath.setText(node.getPkg());
        rightInfoTextFieldSize.setText(String.valueOf(jarEntry.getSize()));
        rightInfoTextFieldEntries.setText(String.valueOf(jarFile.size()));
        rightInfoTextFieldComment.setText(jarFile.getComment());

        boolean b = jarFile.isMultiRelease();
        try {
            Map<String, Attributes> entries = jarFile.getManifest().getEntries();
            if(entries != null && entries.size() > 0) {
                // TODO: Metadata from Manifest
            }
        }
        catch (IOException e) {

        }
    }
    
    public void setStage(Stage stage) {
        this.stage = stage;
    }    
}
