/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swesource.arconsole.controller;

import java.net.URL;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author arnie
 */
public class InfoPaneController {
    
    private Stage stage;
    
    @FXML
    private WebView view;
    
    @FXML
    public void okAction(ActionEvent event) {
        stage.close();
    }
    
    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
    
    public void setInfoType(String type) {
        URL url = getClass().getResource("/info/" + type + ".xhtml");
        WebEngine engine = view.getEngine();
        engine.load(url.toExternalForm());
    }
}
