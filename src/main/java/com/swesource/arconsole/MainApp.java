package com.swesource.arconsole;

import java.util.logging.Logger;
import com.swesource.arconsole.controller.FXMLController;
import java.io.File;
import java.util.List;
import java.util.logging.Level;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainApp extends Application {

    private static final Logger LOGGER = Logger.getLogger("com.swesource.arconsole.MainApp");
        
    @Override
    public void start(Stage stage) throws Exception {
        
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/arconsole.fxml"));
        BorderPane root = (BorderPane) loader.load();

        Scene scene = new Scene(root);
        //scene.getStylesheets().add("/styles/Styles.css");
        scene.setOnDragDropped((DragEvent event) -> {
            Dragboard dragboard = event.getDragboard();
            if(dragboard.hasFiles()) {
                List<File> files = dragboard.getFiles();
                if(files.size() != 1) {
                    // Show error message refusing multiple files - dnd only supports one file
                }
                String path = null;
                for(File file : dragboard.getFiles()) {
                    LOGGER.log(Level.INFO, "arconsole DnD: {0}", file.getAbsolutePath());
                    System.out.println("DnD: " + file.getAbsolutePath());
                    event.setDropCompleted(true);
                }
            }
            event.setDropCompleted(false);
            event.consume();
        });
        
        stage.setTitle("arconsole");
        stage.setScene(scene);
        FXMLController controller = (FXMLController) loader.getController();
        controller.setStage(stage);
        stage.show();
    }
    
    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        for(int i = 0 ; i < args.length ; i++) {
            Logger.getLogger("com.swesource.arconsole.MainApp").log(Level.INFO, "arconsole main argument-{0}: {1}", new Object[]{i, args[i]});
        }
        launch(args);
    }
}
