package com.swesource.arconsole.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author arnie
 */
public class Node<T, V> {
    private T entry;
    private V container;
    private boolean leaf;
    private ObservableList<Node<V,T>> subNodes = FXCollections.observableArrayList();
    
    private String name;
    private String pkg;
    private String ext;
    
    public Node(T t, V v) { this.entry = t; this.container = v;}
    public T getEntry() { return this.entry; }
    public void setEntry(T entry) { this.entry = entry; }
    public V getContainer() { return this.container; }
    public void setContainer(V container) { this.container = container; }
    public boolean isLeaf() { return leaf; }
    public void setLeaf(boolean leaf) { this.leaf = leaf; }
    public ObservableList<Node<V,T>> getSubNodes() { return subNodes; }
    public void setSubNodes(ObservableList<Node<V,T>> subNodes) { this.subNodes = subNodes; }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    public String getPkg() { return pkg; }
    public void setPkg(String pkg) { this.pkg = pkg; }
    public String getExt() { return ext; }
    public void setExt(String ext) { this.ext = ext; }
}
