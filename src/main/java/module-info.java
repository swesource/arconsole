module arconsole {
    requires java.logging;
    requires javafx.base;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;
    exports com.swesource.arconsole to javafx.graphics;
    exports com.swesource.arconsole.controller to javafx.fxml;
    opens com.swesource.arconsole.controller to javafx.fxml;
}
